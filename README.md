# `khb`

The repository for the work-in-progress `khb` R package.
It implements the KHB method as described in [Karlson, Holm, and Breen (2012)](https://doi.org/10.1177/0081175012444861).

# Dependencies
The following packages not provided in the base installation of R are required:
- [`systemfit`](https://CRAN.R-project.org/package=systemfit)
- [`sandwich`]( https://CRAN.R-project.org/package=sandwich)

# Installation
The package can be installed using the [`devtools`]( https://CRAN.R-project.org/package=devtools) package with the following
commands:
```{r}
install.packages("devtools")
devtools::install_gitlab(repo = "Dan.Orsholits/khb", host = "gitlab.unige.ch")
```